# artwork
Simple free artwork I made. Https://thealaskalinuxuser.wordpress.com


This artwork found in this art repository is made by me, the Alaskalinuxuser, and is licensed under the CC-BY 4.0 license, which means that you can use it for commercial or noncommercial use, you can modify it and copy it, and redistribute it, provided that you acknowledge that I originally made it.

You can read about it here:

Human readable
https://creativecommons.org/licenses/by/4.0/

Legal code
https://creativecommons.org/licenses/by/4.0/legalcode